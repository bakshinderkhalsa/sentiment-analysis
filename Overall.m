function Overall
    file_name=input('Please Input The File Name:','s');
    [~,text,~]=xlsread(file_name);
    text=text(:,4);
    
    senti=Sentiments_ana(text);
    
    function Sentiment=Sentiments_ana(text)
        
        function scores = Sentimental_score(tweetings,scoreFile,stopURLs)
            
            AFINN = readtable(scoreFile,'Delimiter','\t','ReadVariableNames',0);% load AFINN data
            AFINN.Properties.VariableNames = {'Term','Score'};
            being_tokeniz = tokenize(tweetings,stopURLs); % tokenize tweetings
            scores = zeros(size(being_tokeniz));% compute sentiment scores
            for i = 1:length(being_tokeniz)
                
                scores(i) = sum(AFINN.Score(ismember(AFINN.Term,being_tokeniz{i})));
                if(scores(i)>0)
                    scores(i)=4;
                elseif (scores(i)<0)
                    scores(i)=0;
                else
                    scores(i)=2;
                end
               
                
            end
        end
        
        function [being_tokeniz,V_out] = tokenize(tweetings,stopURLs)
            % load stop words data
            stoppings = urlread(stopURLs);
            stoppings = textscan(stoppings,'%s','Delimiter',',');
            stoppings = stoppings{1}(:);
            
            % remove special characters in tokenization
            delimiters = {' ','$','/','.','-',':','&','*',...
                '+','=','[',']','?','!','(',')','{','}',',',...
                '"','>','_','<',';','%',char(10),char(13)};
            being_tokeniz = cell(size(tweetings));
            word_list = {};
            for i = 1:length(tweetings)
                tweeting = regexprep(tweetings, '[0-9]+',''); % remove numbers
                tweeting = lower(tweeting(i)); % lower case
                tweeting = regexprep(tweeting,'(http|https)://[^\s]*','');  % remove URLs
                % check if tweeting is still valid
                if ~isempty(tweeting)
                    
                    Tokenz = textscan(char(tweeting),'%s','Delimiter',delimiters); % tokenize the content - returns a nested cell array\
                    Tokenz = Tokenz{1}(:);
                    Tokenz = regexprep(Tokenz,'\\u',''); % remove unicode characters
                    Tokenz = Tokenz(~cellfun('isempty',Tokenz));           % remove empty elements
                    Tokenz = Tokenz(~ismember(Tokenz, stoppings)); % remove stopwords
                    files = dir();
                    if ismember('porterStemmer.m',arrayfun(@(x) x.name,files,'UniformOutput',false))
                        % stem the token
                        for k = 1:length(Tokenz)
                            try
                                Tokenz{k} = porterStemmer(strtrim(Tokenz{k}));
                            catch
                                Tokenz{k} = '';
                                continue;
                            end
                        end
                    end
                    
                    Tokenz = Tokenz(cellfun('length',Tokenz) > 1); % remove one character words
                    being_tokeniz{i,1} = Tokenz; % store Tokenz
                    word_list = [word_list;Tokenz];
                end
            end
            V_out = {word_list};
        end
        scoreFile = 'AFINN-en-165.txt';% load supporting data for text processing
        stopURLs ='http://www.textfixer.com/resources/common-english-words.txt';
        Sentiment = Sentimental_score(text,scoreFile,stopURLs);
        % calculate and print NSRs
        Net_Senti = (4*sum(Sentiment==4) ...
            +2*sum(Sentiment==2)) ...
            /length(text);
        
        fprintf('Net Sentiment  :  %.2f\n',Net_Senti);
    end
matrix=table(senti,text);
writetable(matrix,'Sentiment_analysis.xlsx','WriteVariableNames',false);
end